﻿namespace ClassLibraryShop;

public class Warehouse
{
    public List<Warehouse> warehouseProducts;

    public IProduct Product { get; set; }

    public string Unit { get; set; }

    public int Number { get; set; }

    public DateTime LastStocked { get; set; }

    public Warehouse(IProduct product, string unit, int number, DateTime lastStocked)
    {
        if (number < 0)
            throw new ArgumentException("Number cannot be negative");

        Unit = unit;
        Product = product;
        Number = number;
        LastStocked = lastStocked;
    }

    public Warehouse() { warehouseProducts = new List<Warehouse>(); }

    public void ReceiptsGoods(IProduct product, string unit, int quantity, DateTime date)
    {
        var existingItem = warehouseProducts.FirstOrDefault(item => item.Product.Name == product.Name);
        if (existingItem != null)
        {
            existingItem.Number += quantity;
            existingItem.LastStocked = date;
        }
        else
        {
            warehouseProducts.Add(new Warehouse(product, unit, quantity, date));
        }
    }

    public void ShipmentGoods(string itemName, int quantity)
    {
        var existingItem = warehouseProducts.FirstOrDefault(item => item.Product.Name == itemName);
        if (existingItem != null)
        {
            if (existingItem.Number >= quantity)
            {
                existingItem.Number -= quantity;
            }
            else
            {
                throw new InvalidOperationException($"Not enough {itemName} in stock.");
            }
        }
        else
        {
            throw new InvalidOperationException($"{itemName} not found in stock.");
        }
    }
}

        