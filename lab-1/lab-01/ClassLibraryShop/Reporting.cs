﻿namespace ClassLibraryShop;

public class Reporting
{
    public void InventoryReport(List<Warehouse> warehouse)
    {

        Console.WriteLine("Inventory Report:");

        foreach (var product in warehouse)
        {
            Console.WriteLine($"{product.Product.Name}: Category: {product.Product.Category}, Count:{product.Number} {product.Unit} (Unit Price: {product.Product.Price.ShowPrice()}) Last Stocked: {product.LastStocked}");
        }
    }

}

