﻿namespace ClassLibraryShop;

public interface IMoney
{
    public int FirstPart { get; set; }

    public int SecondPart { get; set; } 

    public string ShowPrice();
}
