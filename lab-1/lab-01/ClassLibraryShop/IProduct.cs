﻿namespace ClassLibraryShop;

public interface IProduct
{
    string Name { get; }

    string Category { get; }

    IMoney Price { get; }

    void ReducePrice(IMoney amount);
}
