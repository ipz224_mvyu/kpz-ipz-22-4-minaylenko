﻿namespace ClassLibraryShop;

public class Product : IProduct 
{
    public string Name { get; set; }
    public string Category { get; set; }
    public IMoney Price { get; set; }

    public Product(string name, string category, IMoney price )    
    {
        Name = name;
        Category = category;
        Price = price;
    }

    public void ReducePrice(IMoney amount)
    {
        if (Price.FirstPart < amount.FirstPart || (Price.FirstPart == amount.FirstPart && Price.SecondPart <= amount.SecondPart))
            throw new InvalidOperationException("Price cannot be reduced by more than the original price");

        Price.FirstPart -= amount.FirstPart;
        Price.SecondPart -= amount.SecondPart;
        if (Price.SecondPart < 0)
        {
            Price.FirstPart--;
            Price.SecondPart += 100;
        }
    }
}
