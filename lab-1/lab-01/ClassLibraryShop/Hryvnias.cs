﻿namespace ClassLibraryShop;

public class Hryvnias : IMoney
{
    public int FirstPart { get; set; }

    public int SecondPart { get; set; }

    public Hryvnias(int hrivnia, int kop)
    {
        FirstPart = hrivnia;
        SecondPart = kop;
    }

    public string ShowPrice()
    {
        return $"{FirstPart} грн. {SecondPart} копійок";
    }
}
