﻿namespace ClassLibraryShop;

public class EURO : IMoney
{
    public int FirstPart { get; set; }

    public int SecondPart { get; set; }

    public EURO(int euro, int cents)
    {
        FirstPart = euro;
        SecondPart = cents;
    }

    public string ShowPrice()
    {
        return $"{FirstPart} € {SecondPart} cents";
    }
}
