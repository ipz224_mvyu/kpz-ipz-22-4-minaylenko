﻿namespace ClassLibraryShop;

public class Dollars : IMoney 
{
    public int FirstPart { get; set; }

    public int SecondPart { get; set; }

    public Dollars(int dollars, int cents) 
    {
        FirstPart = dollars;
        SecondPart = cents;
    }
    
    public string ShowPrice()
    {
        return $"{FirstPart}$ {SecondPart} cents";
    }
} 