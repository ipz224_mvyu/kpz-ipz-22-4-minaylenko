﻿using ClassLibraryShop;
using System.Text;

namespace shop;

internal class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.Unicode;
        Console.InputEncoding = Encoding.Unicode;

        Reporting reporting = new Reporting();

        Product product1 = new Product("TV", "Machinery", new Hryvnias(40000, 50));
        Product product2 = new Product("Laptop", "Machinery", new Dollars(500, 49));
        Product product3 = new Product("Smartphone", "Machinery", new EURO(1000, 59));

        Console.WriteLine("All Products:");
        Console.WriteLine($"{product1.Name} - Category: {product1.Category}, Price: {product1.Price.ShowPrice()}");
        Console.WriteLine($"{product2.Name} - Category: {product2.Category}, Price: {product2.Price.ShowPrice()}");
        Console.WriteLine($"{product3.Name} - Category: {product3.Category}, Price: {product3.Price.ShowPrice()}");
        Console.WriteLine();

        Warehouse warehouse = new Warehouse();

        warehouse.ReceiptsGoods(product1, "pcs.", 50, DateTime.Now);
        warehouse.ReceiptsGoods(product2, "pcs.", 20, DateTime.Now);
        warehouse.ReceiptsGoods(product3, "pcs.", 10, DateTime.Now);


        reporting.InventoryReport(warehouse.warehouseProducts);

        product1.ReducePrice(new Hryvnias(2000, 29));

        warehouse.ShipmentGoods("TV", 30);
        Console.WriteLine();

        reporting.InventoryReport(warehouse.warehouseProducts);
    }
}
