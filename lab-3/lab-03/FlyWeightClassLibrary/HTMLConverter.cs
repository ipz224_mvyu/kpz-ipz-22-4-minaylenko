﻿using CompositeClassLibrary;
using System.Text;

namespace FlyWeightClassLibrary
{
    public class HTMLConverter
    {
        private readonly List<string> _lines;

        public HTMLConverter(List<string> lines)
        {
            _lines = lines;
        }


        public LightElementNode ConvertTextToHTML(string text)
        {
            string[] lines = string.IsNullOrWhiteSpace(text) ? new string[0] : text.Split('\n');
            var rootNode = new LightElementNode("div", "block", new List<string>(), new List<LightNode>());

            rootNode.Children.Add(new LightElementNode("h1", "block", new List<string>(), new List<LightNode> { new LightTextNode($"{lines[0].Trim()} \n") }));

            for (int i = 1; i < lines.Length; i++)
            {
                string line = lines[i].TrimEnd();
                LightNode node;

                if (string.IsNullOrWhiteSpace(line))
                {
                    node = new LightElementNode("p", "block", new List<string>(), new List<LightNode> { new LightTextNode("<br>  \n") });
                }
                else if (char.IsWhiteSpace(line[0]))
                {
                    node = new LightElementNode("blockquote", "block", new List<string>(), new List<LightNode> { new LightTextNode($" {line} \n") });
                }
                else if (line.Length < 20)
                {
                    node = new LightElementNode("h2", "block", new List<string>(), new List<LightNode> { new LightTextNode($" {line} \n") });
                }
                else
                {
                    node = new LightElementNode("p", "block", new List<string>(), new List<LightNode> { new LightTextNode($"{line} \n") });
                }

                rootNode.Children.Add(node);
            }

            return rootNode;
        }

        // Метод з використанням легковаговика
        public static LightElementNode ConvertTextToHTMLWithLW(string text)
        {
            string[] lines = string.IsNullOrWhiteSpace(text) ? new string[0] : text.Split('\n');
            var rootNode = LightElementNode.GetElementNode("div", "block", new List<string>(), new List<LightNode>());

            rootNode.Children.Add(LightElementNode.GetElementNode("h1", "block", new List<string>(), new List<LightNode> { new LightTextNode($"{lines[0].Trim()} \n") }));

            for (int i = 1; i < lines.Length; i++)
            {
                string line = lines[i].TrimEnd();
                LightNode node;

                if (string.IsNullOrWhiteSpace(line))
                {
                    node = LightElementNode.GetElementNode("p", "block", new List<string>(), new List<LightNode> { new LightTextNode("<br>  \n") });
                }
                else if (char.IsWhiteSpace(line[0]))
                {
                    node = LightElementNode.GetElementNode("blockquote", "block", new List<string>(), new List<LightNode> { new LightTextNode($" {line} \n") });
                }
                else if (line.Length < 20)
                {
                    node = LightElementNode.GetElementNode("h2", "block", new List<string>(), new List<LightNode> { new LightTextNode($" {line} \n") });
                }
                else
                {
                    node = LightElementNode.GetElementNode("p", "block", new List<string>(), new List<LightNode> { new LightTextNode($"{line} \n") });
                }

                rootNode.Children.Add(node);
            }

            return rootNode;
        }
    }
}
