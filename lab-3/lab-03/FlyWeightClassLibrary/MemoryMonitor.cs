﻿using System.Diagnostics;

namespace FlyWeightClassLibrary;

public static class MemoryMonitor
{
    public static void CheckCUrrentProcces()
    {
        Process proc = Process.GetCurrentProcess();
        Console.WriteLine($"Allocated Private Memory: {proc.PrivateMemorySize64 / 1024} KB");
        Console.WriteLine($"Physical Memory Usage: {proc.WorkingSet64 / 1024} KB");
    }
}
