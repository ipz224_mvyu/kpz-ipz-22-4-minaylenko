﻿using CompositeClassLibrary;

namespace FlyWeightClassLibrary;

public class LightElementNode : CompositeClassLibrary.LightElementNode
{
    private static Dictionary<string, LightElementNode> _flyweightPool = new Dictionary<string, LightElementNode>();

    public LightElementNode(string tagName, string closingType, List<string> cssClasses, List<LightNode> childNodes) : base(tagName, closingType, cssClasses, childNodes)
    {

    }

    public static LightElementNode GetElementNode(string tagName, string closingType, List<string> cssClasses, List<LightNode> childNodes)
    {
        string key = $"{tagName}_{closingType}_{string.Join(",", cssClasses)}";

        if (!_flyweightPool.ContainsKey(key))
        {
            _flyweightPool[key] = new LightElementNode(tagName, closingType, cssClasses, childNodes);
        }

        return _flyweightPool[key];
    }

}
