﻿using BridgeClassLibrary;

namespace BridgeConsole;

internal class Program
{
    static void Main(string[] args)
    {
        IRender rasterRenderer = new RasterRender();
        IRender vectorRenderer = new VectorRender();

        Shape circle = new Circle(rasterRenderer);
        Shape square = new Square(vectorRenderer);
        Shape triangleR = new Triangle(rasterRenderer);
        Shape triangleV = new Triangle(vectorRenderer);
        
        circle.Draw();
        square.Draw();
        triangleR.Draw();
        triangleV.Draw();
    }
}