﻿using System.Text;

namespace CompositeClassLibrary;

public class LightElementNode : LightNode
{
    public string TagName;
    public string ClosingType;
    public List<LightNode> Children;
    public List<string> CssClasses;

    public LightElementNode(string tagName, string closingType, List<string> cssClasses, List<LightNode> childNodes)
    {
        TagName = tagName;
        ClosingType = closingType;
        Children = childNodes;
        CssClasses = cssClasses;
    }

    public void AddChild(LightNode child)
    {
        Children.Add(child);
    }

    public override string OuterHTML
    {
        get
        {
            string attributes = CssClasses.Any() ? $" class=\"{string.Join(" ", CssClasses)}\"" : "";
            string childrenHTML = string.Concat(Children.Select(node => node.OuterHTML));

            return ClosingType == "selfClosing" ? $"<{TagName}{attributes}/>" : $"<{TagName}{attributes}>{childrenHTML}</{TagName}>";

        }
    }

    public override string InnerHTML => string.Concat(Children.Select(child => child.OuterHTML));
}
