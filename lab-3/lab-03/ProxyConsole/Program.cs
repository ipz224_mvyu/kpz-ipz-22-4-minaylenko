﻿using ProxyClassLibrary;

namespace ProxyConsole;

internal class Program
{
    static void Main(string[] args)
    {
        string filePath1 = "file1.txt";
        string filePath2 = "file2.txt";

        File.WriteAllText(filePath1, "Hello\nWorld!");
        File.WriteAllText(filePath2, "World\nHello!\nHello!");

        Console.WriteLine("-- SmartTextChecker --\n");
        ITextReader reader1 = new SmartTextChecker(new SmartTextReader());
        reader1.ReadText(filePath1);
        reader1.ReadText(filePath2);

        Console.WriteLine("\n-- SmartTextReaderLocker --\n");
        ITextReader reader2 = new SmartTextReaderLocker("file1.*");
        reader2.ReadText(filePath1);
        reader2.ReadText(filePath2);

        File.Delete(filePath1);
        File.Delete(filePath2);

    }
}