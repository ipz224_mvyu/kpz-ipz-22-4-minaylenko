﻿namespace ProxyClassLibrary;

public class SmartTextChecker : ITextReader
{
    private ITextReader _textReader;

    public SmartTextChecker(ITextReader result)
    {
        _textReader = result;
    }

    public char[][] ReadText(string filePath)
    {
        Console.WriteLine($"Opening file: {filePath}");
        char[][] result = _textReader.ReadText(filePath);
        if (result == null)
        {
            Console.WriteLine($"Error: Failed to read file {filePath}");
            return null;
        }
        Console.WriteLine($"File read successfully");
        Console.WriteLine($"Total lines: {result.Length}");
        int totalCharacters = 0;
        foreach (char[] line in result)
        {
            totalCharacters += line.Length;
        }
        Console.WriteLine($"Total characters: {totalCharacters}");
        Console.WriteLine($"Closing file: {filePath}");

        return result;
    } 
}
