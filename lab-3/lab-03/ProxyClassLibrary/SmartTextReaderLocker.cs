﻿using System.Text.RegularExpressions;

namespace ProxyClassLibrary;

public class SmartTextReaderLocker : ITextReader
{
    private SmartTextReader _reader;
    private SmartTextChecker _checkerreader;
    private Regex _regex;

    public SmartTextReaderLocker(string pattern)
    {
        _reader = new SmartTextReader();
        _checkerreader = new SmartTextChecker(_reader);
        _regex = new Regex(pattern);
    }

    public char[][] ReadText(string filePath)
    {
        if (_regex.IsMatch(filePath))
        {

            Console.WriteLine("Access denied!");
            return null;
        }
        else
        {
            return _checkerreader.ReadText(filePath);
        }
    }

}
