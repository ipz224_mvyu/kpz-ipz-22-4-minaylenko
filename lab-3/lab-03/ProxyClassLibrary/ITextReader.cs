﻿namespace ProxyClassLibrary;

public interface ITextReader
{
    char[][] ReadText(string filePath);
}
