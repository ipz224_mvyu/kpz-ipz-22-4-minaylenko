﻿namespace AdapterClassLibrary;

public class FileWriterAdapter : ILogger
{
    private FileWriter fileWriter;

    public FileWriterAdapter(FileWriter fileWriter)
    {
        this.fileWriter = fileWriter;
    }

    public void Log(string message)
    {
        fileWriter.WriteLine($"[*Log*] {message}");
    }

    public void Error(string message)
    {
        fileWriter.Write($"[*Error*] {message}");
    }

    public void Warn(string message)
    {
        fileWriter.WriteLine($"[*Warning*] {message}");
    }
}
