﻿namespace AdapterClassLibrary;

public class Logger
{
    public void Log(string message)
    {
        ColorIndicator(message, ConsoleColor.Green);
    }

    public void Error(string message)
    {
        ColorIndicator(message, ConsoleColor.Red);
    }

    public void Warn(string message)
    {
        ColorIndicator(message, ConsoleColor.DarkYellow);
    }
    public void ColorIndicator(string message, ConsoleColor color)
    {
        Console.ForegroundColor = color;
        Console.WriteLine(message);
        Console.ResetColor();
    }

}
