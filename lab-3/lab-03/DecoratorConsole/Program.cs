﻿using HeroClassLibrary;

namespace DecoratorConsole
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IHero warrior = new Warrior("Warrior");
            IHero mage = new Mage("Mage");
            IHero paladin = new Palladin("Palladin");

            warrior = new ClothesDecorator(warrior, "Mantle");
            warrior = new WeaponDecorator(warrior, "Mace");

            mage = new WeaponDecorator(mage, "Sword");
            mage = new ArtifactDecorator(mage, "Totem");

            paladin = new ClothesDecorator(paladin, "Mantle");
            paladin = new WeaponDecorator(paladin, "Axe");
            paladin = new ArtifactDecorator(paladin, "totem");

            Console.WriteLine("Warrior:");
            warrior.Show();

            Console.WriteLine("\nMage:");
            mage.Show();

            Console.WriteLine("\nPaladin:");
            paladin.Show();
        }
    }
}