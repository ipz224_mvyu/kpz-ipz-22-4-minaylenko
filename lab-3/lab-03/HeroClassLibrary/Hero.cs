﻿namespace HeroClassLibrary;

public class Hero : IHero
{
    public string Name { get; set; }

    public Hero(string name)
    {
        Name = name;
    }
    public virtual void Show()
    {
        Console.WriteLine($"Hero: {Name}");
    }
}
