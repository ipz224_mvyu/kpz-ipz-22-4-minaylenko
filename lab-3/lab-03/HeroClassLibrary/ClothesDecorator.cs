﻿namespace HeroClassLibrary;

public class ClothesDecorator : BaseDecorator
{
    public string Clothe { get; set; }
    public ClothesDecorator(IHero hero, string clothe) : base(hero) 
    {
        Clothe = clothe;
    }

    public override void Show()
    {
        base.hero.Show();
        Console.WriteLine($"Clothe: {Clothe}");
    }
}
