﻿namespace HeroClassLibrary;

public class WeaponDecorator : BaseDecorator
{
    public string Weapon { get; set; }
    public WeaponDecorator(IHero hero, string weapon) : base(hero) 
    {
        Weapon = weapon;
    }

    public override void Show()
    {
        base.hero.Show();
        Console.WriteLine($"Weapon: {Weapon}");
    }
}
