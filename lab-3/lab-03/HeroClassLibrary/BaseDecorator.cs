﻿namespace HeroClassLibrary;

public abstract class BaseDecorator : IHero
{
    protected IHero hero;

    public BaseDecorator(IHero hero)
    {
        this.hero = hero;
    }

    abstract public void Show();
}
