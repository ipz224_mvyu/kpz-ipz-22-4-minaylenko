﻿namespace HeroClassLibrary;

public class ArtifactDecorator : BaseDecorator
{
    public string Artifact { get; set; }
    public ArtifactDecorator(IHero hero, string artifact) : base(hero) 
    { 
        Artifact = artifact;
    }

    public override void Show()
    {
        base.hero.Show();
        Console.WriteLine($"Clothe: {Artifact}");
    }
}
