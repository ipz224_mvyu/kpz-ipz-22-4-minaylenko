﻿using CompositeClassLibrary;

namespace CompositeConsole;

internal class Program
{
    static void Main(string[] args)
    {
        LightElementNode table = new LightElementNode("table", "closing", new List<string>(), new List<LightNode>());
        LightElementNode row1 = new LightElementNode("tr", "closing", new List<string>(), new List<LightNode>());
        LightElementNode row2 = new LightElementNode("tr", "closing", new List<string>(), new List<LightNode>());

        LightElementNode cell1_row1 = new LightElementNode("td", "closing", new List<string>(), new List<LightNode>());
        LightElementNode cell2_row1 = new LightElementNode("td", "closing", new List<string>(), new List<LightNode>());
        LightElementNode cell1_row2 = new LightElementNode("td", "closing", new List<string>(), new List<LightNode>());
        LightElementNode cell2_row2 = new LightElementNode("td", "closing", new List<string>(), new List<LightNode>());

        LightTextNode text1_row1 = new LightTextNode("Cell 1, Row 1");
        LightTextNode text2_row1 = new LightTextNode("Cell 2, Row 1");
        LightTextNode text1_row2 = new LightTextNode("Cell 1, Row 2");
        LightTextNode text2_row2 = new LightTextNode("Cell 2, Row 2");

        cell1_row1.AddChild(text1_row1);
        cell2_row1.AddChild(text2_row1);
        cell1_row2.AddChild(text1_row2);
        cell2_row2.AddChild(text2_row2);

        row1.AddChild(cell1_row1);
        row1.AddChild(cell2_row1);
        row2.AddChild(cell1_row2);
        row2.AddChild(cell2_row2);

        table.AddChild(row1);
        table.AddChild(row2);

        Console.WriteLine("Outer HTML of table:");
        Console.WriteLine(table.OuterHTML);
        Console.WriteLine();

        Console.WriteLine("Inner HTML of table:");
        Console.WriteLine(table.InnerHTML);
    }
}