﻿using FlyWeightClassLibrary;

namespace FlyWeightConsole;

internal class Program
{
    static void Main(string[] args)
    {
        string filePath = @"D:\My_\Institut\KPZ\lab-3\lab-03\text-task06.txt";

        try
        {
            List<string> lines = File.ReadAllLines(filePath).ToList();

            HTMLConverter converter = new HTMLConverter(lines);

            Console.WriteLine("\n-- With FlyWeight --");
            LightElementNode htmlContentWithFlyWeight = HTMLConverter.ConvertTextToHTMLWithLW(string.Join("\n", lines));
            MemoryMonitor.CheckCUrrentProcces();

            Console.WriteLine("\n-- Without FlyWeight --\n");
            LightElementNode htmlContentWithoutFlyWeight = converter.ConvertTextToHTML(string.Join("\n", lines));
            MemoryMonitor.CheckCUrrentProcces();

        }
        catch (Exception ex)
        {
            Console.WriteLine($"An error occurred: {ex.Message}");
        }
    }
}