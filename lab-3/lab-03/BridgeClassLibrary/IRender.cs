﻿namespace BridgeClassLibrary;

public interface IRender
{
    void Render(string shape);
}
