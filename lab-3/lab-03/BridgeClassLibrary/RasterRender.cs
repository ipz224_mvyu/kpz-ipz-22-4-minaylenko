﻿namespace BridgeClassLibrary;

public class RasterRender : IRender
{
    public void Render(string shape)
    {
        Console.WriteLine($"{shape} as pixel graphics");
    }
}
