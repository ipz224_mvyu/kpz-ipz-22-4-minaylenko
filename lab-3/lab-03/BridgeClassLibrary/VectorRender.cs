﻿namespace BridgeClassLibrary;

public class VectorRender : IRender
{
    public void Render(string shape)
    {
        Console.WriteLine($"{shape} as vector graphics");
    }   
}
