﻿using AdapterClassLibrary;

namespace AdapterConsole
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Logger logger = new Logger();
            string filePath = @"D:\My_\Institut\KPZ\lab-3\lab-03\log.txt";
            FileWriter fileWriter = new FileWriter(filePath);

            ILogger fileLogger = new FileWriterAdapter(fileWriter);

            logger.Log("This is a log message");
            logger.Error("This is an error message");
            logger.Warn("This is a warning message");

            fileLogger.Log("This is a log message to file");
            fileLogger.Error("This is an error message to file");
            fileLogger.Warn("This is a warning message to file");
        }
    }
}