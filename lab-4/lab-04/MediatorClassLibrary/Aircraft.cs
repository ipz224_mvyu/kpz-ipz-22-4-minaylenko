﻿namespace MediatorClassLibrary;

public class Aircraft
{
    public string Name { get; }
    public Runway? CurrentRunway { get; set; }

    public Aircraft(string name)
    {
        Name = name;
    }

    public void Land()
    {
        Console.WriteLine($"Літак {Name} приземлився.");
    }

    public void TakeOff()
    {
        Console.WriteLine($"Літак {Name} злетів.");
    }
}