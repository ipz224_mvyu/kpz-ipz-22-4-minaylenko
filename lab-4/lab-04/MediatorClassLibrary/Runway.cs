﻿namespace MediatorClassLibrary;

public class Runway
{
    public readonly Guid Id = Guid.NewGuid();
    public string Number { get; private set; }
    public Aircraft? IsBusyWithAircraft { get; private set; }

    public Runway(string number)
    {
        Number = number;
    }

    public bool IsAvailable()
    {
        return IsBusyWithAircraft == null;
    }

    public void LandAircraft(Aircraft aircraft)
    {
        if (IsAvailable())
        {
            IsBusyWithAircraft = aircraft;
            aircraft.CurrentRunway = this;
            aircraft.Land();
        }
        else
        {
            Console.WriteLine($"Доріжка {Number} зайнята. Літак {aircraft.Name} не може приземлитися.");
        }
    }

    public void ReleaseAircraft()
    {
        if (IsBusyWithAircraft != null)
        {
            Console.WriteLine($"Доріжка {Number} вільна. Літак {IsBusyWithAircraft.Name} злетів.");
            IsBusyWithAircraft.CurrentRunway = null;
            IsBusyWithAircraft = null;
        }
    }

}
