﻿namespace MediatorClassLibrary;

public class CommandCentre
{
    private List<Runway> _runways = new List<Runway>();
    private List<Aircraft> _aircrafts = new List<Aircraft>();

    public CommandCentre(List<Runway> runways, List<Aircraft> aircrafts)
    {
        _runways = runways;
        _aircrafts = aircrafts;
    }

    public void AircraftLandingInquiry(Aircraft aircraft)
    {
        Console.WriteLine($"Літак {aircraft.Name} запитує посадку.");

        Runway availableRunway = _runways.FirstOrDefault(r => r.IsAvailable());
        if (availableRunway != null)
        {
            Console.WriteLine($"Полоса {availableRunway.Number} вільна. Літак {aircraft.Name} приземляється.");
            availableRunway.LandAircraft(aircraft);
        }
        else
        {
            Console.WriteLine($"Всі доріжки зайняті. Літак {aircraft.Name} не може приземлитися.");
        }
        Console.WriteLine();
    }

    public void AircraftTakeOffInquiry(Aircraft aircraft)
    {
        Console.WriteLine($"Літак {aircraft.Name} запитує зліт.");

        Runway currentRunway = aircraft.CurrentRunway;
        if (currentRunway != null && currentRunway.IsBusyWithAircraft == aircraft)
        {
            Console.WriteLine($"Літак {aircraft.Name} злітає з доріжки {currentRunway.Number}.");
            currentRunway.ReleaseAircraft();
        }
        else
        {
            Console.WriteLine($"Літак {aircraft.Name} не знаходиться на жодній доріжці. Неможливо злетіти.");
        }
        Console.WriteLine();
    }
}
