﻿using ChainOfResponsClassLibrary;
using System.Text;

namespace ChainOfResponsConsole;

internal class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.Unicode;
        Console.InputEncoding = Encoding.Unicode;

        var problemLevel = new FirstSupportHandler();

        problemLevel.SetNextHandler(new SecondSupportHandler())
            .SetNextHandler(new ThirdSupportHandler())
            .SetNextHandler(new FourthSupportHandler())
            .SetNextHandler(new FifthSupportHandler());

        string[] userRequests = {
                "Проблема з підключенням",
                "Проблема налаштування",
                "Проблема з мережею",
                "Проблема з програмним забезпеченням",
                "Складні проблеми, що потребують підтримки"
        };

        foreach (var request in userRequests)
        {
            Console.WriteLine($"Запит користувача: {request}");
            Console.WriteLine($"Результат обробки: {problemLevel.Handle(request)}");
            Console.WriteLine();
        }
    }
}