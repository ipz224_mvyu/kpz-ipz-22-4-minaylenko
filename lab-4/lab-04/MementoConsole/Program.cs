﻿using MementoClassLibrary;
using System.Text;

namespace MementoConsole;

internal class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.Unicode;
        Console.InputEncoding = Encoding.Unicode;

        TextEditor editor = new TextEditor();

        editor.SetContent("Hello world!");
        editor.Save();
        Console.WriteLine("Поточний вміст: " + editor.GetContent());

        editor.SetContent("By world!");
        editor.Save();
        Console.WriteLine("Поточний вміст: " + editor.GetContent());

        editor.Undo();
        Console.WriteLine("Вміст після скасування: " + editor.GetContent());

        editor.Undo();
        Console.WriteLine("Вміст після скасування: " + editor.GetContent());

        editor.Undo();
        Console.WriteLine("Вміст після ще одного скасування: " + editor.GetContent());
    }
}