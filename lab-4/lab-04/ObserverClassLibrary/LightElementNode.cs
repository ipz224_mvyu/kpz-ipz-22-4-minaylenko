﻿namespace ObserverClassLibrary;

public class LightElementNode : LightNode
{
    public string TagName { get; }
    public List<string> CSSClasses { get; }
    public List<LightNode> ChildNodes { get; }
    private Observer _observer = new Observer();

    public LightElementNode(string tagName, List<string> cssClasses, List<LightNode> childNodes)
    {
        TagName = tagName;
        CSSClasses = cssClasses;
        ChildNodes = childNodes;
    }

    public void AddEventListener(string eventType, Action listener)
    {
        _observer.Subscribe(eventType, listener);
    }

    public void RemoveEventListener(string eventType, Action listener)
    {
        _observer.Unsubscribe(eventType, listener);
    }

    public void TriggerEvent(string eventType)
    {
        _observer.TriggerEvent(eventType);
    }

    public override string OuterHTML
    {
        get
        {
            string cssClassStr = string.Join(" ", CSSClasses);
            string attributes = $"class=\"{cssClassStr}\"";
            string childrenHTML = string.Join("", ChildNodes.Select(node => node.OuterHTML));

            return $"<{TagName} {attributes}>{childrenHTML}</{TagName}>";
        }
    }

    public override string InnerHTML
    {
        get
        {
            return string.Join("", ChildNodes.Select(node => node.OuterHTML));
        }
    }
}
