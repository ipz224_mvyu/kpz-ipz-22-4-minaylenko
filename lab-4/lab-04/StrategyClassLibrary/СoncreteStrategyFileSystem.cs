﻿namespace StrategyClassLibrary;

public class СoncreteStrategyFileSystem : IImageLoadingStrategy
{
    public byte[]? LoadImage(string href)
    {
        Console.WriteLine($"Зображення завантажено з файлової системи.\n Посилання: {href}");

        return File.ReadAllBytes(href);
    }

}
