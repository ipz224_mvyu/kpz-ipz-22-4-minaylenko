﻿using System.Net;

namespace StrategyClassLibrary;

public class ConcreteStrategyNetwork : IImageLoadingStrategy
{
    public byte[]? LoadImage(string href)
    {
        try
        {
            using (WebClient webClient = new WebClient())
            {
                Console.WriteLine($"Зображення завантажено з мережі системи.\n Посилання: {href}");

                return webClient.DownloadData(href);
            }
        }
        catch (WebException ex)
        {
            Console.WriteLine($"Помилка при завантаженні зображення з мережі: {ex.Message}");
            return null;
        }
    }
}
