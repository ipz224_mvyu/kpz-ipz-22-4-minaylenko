﻿namespace ChainOfResponsClassLibrary;

public class FourthSupportHandler : BaseSupportHandler
{
    protected override string Answer => "-- Перевірте програмне забезпечення та встановлені додатки --";
    protected override string HandableError => "Проблема з програмним забезпечення";

    public override string Handle(string userRequest)
    {
        if (userRequest.Contains(HandableError))
        {
            Console.WriteLine($"Рівень підтримки 4: {Answer}");
            return "Проблема вирішена на рівні 4!";
        }
        else
        {
            return base.Handle(userRequest);
        }
    }
}
