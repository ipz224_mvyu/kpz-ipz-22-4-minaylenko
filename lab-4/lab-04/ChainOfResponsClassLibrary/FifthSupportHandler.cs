﻿namespace ChainOfResponsClassLibrary;

public class FifthSupportHandler : BaseSupportHandler
{
    protected override string Answer => "-- Зверніться до відділу технічної підтримки для подальшої допомоги --";
    protected override string HandableError => "Складні проблеми, що потребують підтримки";

    public override string Handle(string userRequest)
    {
        if (userRequest.Contains(HandableError))
        {
            Console.WriteLine($"Рівень підтримки 5: {Answer}");
            return "Проблема вирішена на рівні 5!";
        }
        else
        {
            return base.Handle(userRequest);
        }
    }
}
