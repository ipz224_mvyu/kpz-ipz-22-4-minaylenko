﻿namespace ChainOfResponsClassLibrary;

public class ThirdSupportHandler : BaseSupportHandler
{
    protected override string Answer => "-- Перевірте мережу та налаштування --";
    protected override string HandableError => "Проблема з мережею";

    public override string Handle(string userRequest)
    {
        if (userRequest.Contains(HandableError))
        {
            Console.WriteLine($"Рівень підтримки 3: {Answer}");
            return "Проблема вирішена на рівні 3!";
        }
        else
        {
            return base.Handle(userRequest);
        }
    }
}
