﻿namespace ChainOfResponsClassLibrary;

public class FirstSupportHandler : BaseSupportHandler
{
    protected override string Answer => "-- Перевірте наявність оновлень та перезавантажте пристрій --";

    protected override string HandableError => "Проблема з підключенням";

    public override string Handle(string userRequest)
    {
        if (userRequest.Contains(HandableError))
        {
            Console.WriteLine($"Рівень підтримки 1: {Answer}");
            return "Проблема вирішена на рівні 1!";
        }
        else
        {
            return base.Handle(userRequest);
        }
    }
}
