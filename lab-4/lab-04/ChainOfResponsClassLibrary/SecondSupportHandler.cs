﻿namespace ChainOfResponsClassLibrary;

public class SecondSupportHandler : BaseSupportHandler
{
    protected override string Answer => "-- Перевірте налаштування та здійсніть перезавантаження пристрою --";
    protected override string HandableError => "Проблема налаштування";

    public override string Handle(string userRequest)
    {
        if (userRequest.Contains(HandableError))
        {
            Console.WriteLine($"Рівень підтримки 2: {Answer}");
            return "Проблема вирішена на рівні 2!";
        }
        else
        {
            return base.Handle(userRequest);
        }
    }
}
