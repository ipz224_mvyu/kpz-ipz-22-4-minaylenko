﻿namespace ChainOfResponsClassLibrary;

public abstract class BaseSupportHandler : IHandler
{
    protected abstract string Answer { get; }
    protected abstract string HandableError { get; }


    private IHandler? _nextHandler;

    public BaseSupportHandler SetNextHandler(BaseSupportHandler nextHandler)
    {
        this._nextHandler = nextHandler;
        return nextHandler;
    }

    public virtual string Handle(string userRequest)
    {
        if (_nextHandler == null)
        {
            return userRequest;
        }
        else
        {
            return this._nextHandler.Handle(userRequest);
        }
    }
}
