﻿namespace ChainOfResponsClassLibrary;

public interface IHandler
{
    public string Handle(string userRequest);
}
