﻿using StrategyClassLibrary;
using System.Text;

namespace StrategyConsole;

internal class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.Unicode;
        Console.InputEncoding = Encoding.Unicode;

        var fileSystemLoader = new СoncreteStrategyFileSystem();
        var networkLoader = new ConcreteStrategyNetwork();

        var image = new Image(fileSystemLoader);

        image.Load("D:\\My_\\Institut\\KPZ\\lab-4\\lab-04\\photo.jpg");

        image.SetLoadingStrategy(networkLoader);

        image.Load("https://masterzoo.ua/content/uploads/images/enot-masterzoo-ua.png");
    }
}