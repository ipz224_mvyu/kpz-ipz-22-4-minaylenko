﻿using ObserverClassLibrary;

namespace ObserverConsole;

internal class Program
{
    static void Main(string[] args)
    {
        var button = new LightElementNode("button", new List<string> { "btn", "btn-primary" }, new List<LightNode> { new LightTextNode("Click me") });
        Action clickHandler1 = () => Console.WriteLine("Button clicked!");
        button.AddEventListener("click", clickHandler1);
        button.TriggerEvent("click");
        button.TriggerEvent("move");
        button.TriggerEvent("click");
        button.TriggerEvent("doubleclick");
        button.TriggerEvent("click");

        Action clickHandler2 = () => Console.WriteLine("Button doubleclicked!");
        button.AddEventListener("doubleclick", clickHandler2);
        button.TriggerEvent("doubleclick");
        button.TriggerEvent("click");
        button.RemoveEventListener("click", clickHandler1);

        Action clickHandler3 = () => Console.WriteLine("Button moved!");
        button.AddEventListener("move", clickHandler3);
        button.TriggerEvent("move");
        button.TriggerEvent("click");
        button.TriggerEvent("click");
        button.TriggerEvent("click");
    }
}