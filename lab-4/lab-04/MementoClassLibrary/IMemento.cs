﻿namespace MementoClassLibrary;

public interface IMemento
{
    string GetContent();
}
