﻿namespace MementoClassLibrary;

public class HistoryManager : IHistoryManager
{
    private Stack<IMemento> history;

    public HistoryManager()
    {
        history = new Stack<IMemento>();
    }

    public void Save(IMemento document)
    {
        IMemento snapshot = new TextDocument(document.GetContent());
        history.Push(snapshot);
    }

    public IMemento Undo()
    {
        if (history.Count > 0)
        {
            return history.Pop();
        }
        return null;
    }
}
