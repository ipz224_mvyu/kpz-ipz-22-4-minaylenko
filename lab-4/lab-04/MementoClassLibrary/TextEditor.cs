﻿namespace MementoClassLibrary;

public class TextEditor
{
    private IMemento currentDocument;
    private IHistoryManager historyManager;

    public TextEditor()
    {
        currentDocument = new TextDocument("");
        historyManager = new HistoryManager();
    }

    public void Save()
    {
        historyManager.Save(currentDocument);
    }

    public void Undo()
    {
        IMemento previousDocument = historyManager.Undo();
        if (previousDocument != null)
        {
            currentDocument = previousDocument;
            Console.WriteLine("Undo successful.");
        }
        else
        {
            Console.WriteLine("Nothing to undo.");
        }
    }

    public void SetContent(string content)
    {
        currentDocument = new TextDocument(content);
    }

    public string GetContent()
    {
        return currentDocument.GetContent();
    }
}
