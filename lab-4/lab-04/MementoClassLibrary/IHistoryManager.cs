﻿namespace MementoClassLibrary;

public interface IHistoryManager
{
    void Save(IMemento document);
    IMemento Undo();
}
