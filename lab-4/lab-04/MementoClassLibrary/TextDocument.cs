﻿namespace MementoClassLibrary;

public class TextDocument : IMemento
{
    public string Content { get; set; }

    public TextDocument(string content)
    {
        Content = content;
    }

    public string GetContent()
    {
        return Content;
    }
}
