﻿using MediatorClassLibrary;
using System.Text;

namespace MediatorConsole;
internal class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.Unicode;
        Console.InputEncoding = Encoding.Unicode;

        var runways = new List<Runway>
        {
            new Runway("#1"),
            new Runway("#2"),
            new Runway("#3")
        };

        var aircrafts = new List<Aircraft>
        {
            new Aircraft("Боїнг"),
            new Aircraft("Мрія"),
            new Aircraft("Ембраер")
        };

        var commandCentre = new CommandCentre(runways, aircrafts);

        foreach (var aircraft in aircrafts)
        {
            commandCentre.AircraftLandingInquiry(aircraft);

        }

        foreach (var runway in runways)
        {
            runway.LandAircraft(aircrafts[0]);
        }

        foreach (var aircraft in aircrafts)
        {
            commandCentre.AircraftLandingInquiry(aircraft);
            commandCentre.AircraftTakeOffInquiry(aircraft);
        }
    }
}