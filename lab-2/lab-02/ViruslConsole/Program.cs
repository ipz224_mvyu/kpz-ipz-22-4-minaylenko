﻿using VirusLibrary;

namespace ViruslConsole;

internal class Program
{
    static void Main(string[] args)
    {
        Virus mainVirus = new Virus("Main Virus", 129, 10, "Type A", new List<Virus>());
        Virus parentVirus1 = new Virus("Parent Virus 1", 99, 6, "Type B", new List<Virus>());
        Virus parentVirus2 = new Virus("Parent Virus 2", 75, 3, "Type C", new List<Virus>());
        Virus childVirus1 = new Virus("Child Virus 1", 40, 2, "Type D", new List<Virus>());
        Virus childVirus2 = new Virus("Child Virus 2", 30, 1, "Type E", new List<Virus>());
        Virus childVirus3 = new Virus("Child Virus 3", 40, 2, "Type F", new List<Virus>());

        mainVirus.Children.Add(parentVirus1);
        mainVirus.Children.Add(parentVirus2);
        parentVirus1.Children.Add(childVirus1);
        parentVirus1.Children.Add(childVirus2);
        parentVirus2.Children.Add(childVirus3);

        Virus clonedmain = mainVirus.Clone();

        Console.WriteLine("Original Main Virus:");
        PrintVirusInfo(mainVirus);
        Console.WriteLine("\nCloned Main Virus:");
        PrintVirusInfo(clonedmain);
        Console.ReadLine();
    }

    static void PrintVirusInfo(Virus virus)
    {
        Console.WriteLine(virus);
        if (virus.Children.Count > 0)
        {
            Console.WriteLine("Children:");
            foreach (var child in virus.Children)
            {
                PrintVirusInfo(child);
            }
        }
    }
}