﻿using AbstractFactoryLibrary;

namespace FactoryConsole
{
    internal class Program
    {
        private static void ShowInfomationByManufacturer(IDeviceFactory deviceFactory)
        {
            EBook ebook = deviceFactory.CreateEBook();
            Smartphone smartphone = deviceFactory.CreateSmartphone();
            Laptop laptop = deviceFactory.CreateLaptop();
            Netbook netbook = deviceFactory.CreateNetbook();

            Console.WriteLine(ebook.DisplayInfo());
            Console.WriteLine(smartphone.DisplayInfo());
            Console.WriteLine(laptop.DisplayInfo());
            Console.WriteLine(netbook.DisplayInfo());
            Console.WriteLine();
        }

        static void Main(string[] args)
        {
            IDeviceFactory iProneFactory = new IProneFactory();
            IDeviceFactory kiaomiFactory = new KiaomiFactory();
            IDeviceFactory balaxyFactory = new BalaxyFactory();

            ShowInfomationByManufacturer(iProneFactory);
            ShowInfomationByManufacturer(kiaomiFactory);
            ShowInfomationByManufacturer(balaxyFactory);

        }
    }
}