﻿namespace VirusLibrary;

public interface IPrototype
{
    public Virus Clone();
}
