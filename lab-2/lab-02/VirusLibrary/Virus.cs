﻿namespace VirusLibrary;

public class Virus : IPrototype
{
    public string Name { get; set; }
    public double Weight { get; set; }
    public int Age { get; set; }
    public string Type { get; set; }
    public List<Virus> Children { get; set; }
    


    public Virus(string name, double weight, int age, string type, List<Virus> children)
    {
        Name = name;
        Weight = weight;
        Age = age;
        Type = type;
        Children = children;
    }
    
    public Virus Clone()
    {
        return new Virus(Name, Weight, Age, Type, new List<Virus>(Children));
    }

    public override string ToString()
    {
        string childrenInfo = Children != null && Children.Count > 0 ? "Children: " + string.Join(", ", Children.Select(child => child.Name)) : "No Children";
        return $"Name: {Name}, Weight: {Weight}, Age: {Age}, Type: {Type}, {childrenInfo}";
    }
}