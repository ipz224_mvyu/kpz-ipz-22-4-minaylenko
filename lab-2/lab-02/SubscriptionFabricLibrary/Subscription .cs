﻿namespace SubscriptionFabricLibrary;

public abstract class Subscription 
{
    public string Name { get; protected set; }
    public float MonthlyPrice { get; protected set; }
    public int MinSubscriptionPeriod { get; protected set; }
    public List<string> AvailableChannels { get; protected set; }

    public Subscription(float monthlyPrice, int minSubscriptionPeriod, List<string> availableChannels)
    {
        MonthlyPrice = monthlyPrice;
        MinSubscriptionPeriod = minSubscriptionPeriod;
        AvailableChannels = availableChannels;
    }

    public virtual void DisplaySubscriptionInfo()
    {
        Console.WriteLine($"Subscription Type: {GetType().Name}");
        Console.WriteLine($"Monthly Price: {MonthlyPrice}");
        Console.WriteLine($"Minimum Subscription Period: {MinSubscriptionPeriod}");
        Console.WriteLine("Available Channels:");
        foreach (var channel in AvailableChannels)
        {
            Console.WriteLine($"| - {channel}");
        }
    }
}