﻿namespace SubscriptionFabricLibrary;

public class WebSite : ISubscriptionCreator
{
    public Subscription CreateSubscription()
    {
        return new DomesticSubscription();
    }
}
