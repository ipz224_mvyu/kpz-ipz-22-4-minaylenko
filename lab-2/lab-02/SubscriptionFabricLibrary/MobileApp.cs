﻿namespace SubscriptionFabricLibrary;

public class MobileApp : ISubscriptionCreator
{
    public Subscription CreateSubscription()
    {
        return new PremiumSubscription();
    }
}
