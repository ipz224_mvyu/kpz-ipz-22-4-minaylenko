﻿namespace SubscriptionFabricLibrary;

public interface ISubscriptionCreator
{
    Subscription CreateSubscription();

}
