﻿namespace SubscriptionFabricLibrary;

public class DomesticSubscription : Subscription
{
    public DomesticSubscription() : base(9.99f, 1, new List<string> { "Basic", "News", "Sports" }) { }
}
