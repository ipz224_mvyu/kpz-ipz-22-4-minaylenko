﻿namespace SubscriptionFabricLibrary;

public class ManagerCall : ISubscriptionCreator
{
    public Subscription CreateSubscription()
    {
        return new EducationalSubscription();
    }
}
