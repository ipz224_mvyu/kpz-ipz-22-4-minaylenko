﻿namespace SubscriptionFabricLibrary;

public class EducationalSubscription : Subscription
{
    public EducationalSubscription() : base(19.99f, 6, new List<string> { "Basic", "C# Developer", "Educational", "Documentry" }) { }
}
