﻿namespace SubscriptionFabricLibrary;

public class PremiumSubscription : Subscription
{
    public PremiumSubscription() : base(29.99f, 6, new List<string> { "Basic", "News", "Sports", "C# Developer", "Pictures", "Movies" }) { }
}
