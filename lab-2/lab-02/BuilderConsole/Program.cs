﻿using BuilderLibrary;

namespace BuilderConsole;

internal class Program
{
    static void Main(string[] args)
    {
        var director = new CharacterDirector();
        var characterBuilder = new HeroBuilder();
        var enemyBuilder = new EnemyBuilder();
        director.CharacterBuilder = characterBuilder;

        var hero = director.ConstructCharacter(1.80, "Athletic", "Blonde", "Blue", "Armor", new List<string> { "Sword", "Shield" }, "FireBall", 10);

        director.CharacterBuilder = enemyBuilder;
        var enemy = director.ConstructEnemy(2.00, "Muscular", "Black", "Red", "Dark Robe", new List<string> { "Dagger", "Poison" }, "FireBreath", 10, new List<string> { "Destroying villages", "Spreading chaos" }, "Fierce and cunning");

        Console.WriteLine("Hero:");
        Console.WriteLine(hero.DisplayInfo());
        Console.WriteLine("Enemy:");
        Console.WriteLine(enemy.DisplayInfo() + $"| EvilDeeds: {string.Join(", ", enemy.EvilDeeds)}\n" + $"| EvilCharacteristics: {enemy.EvilCharacteristics}");
    }
}