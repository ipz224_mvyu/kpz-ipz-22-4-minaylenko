﻿namespace SingletonLibrary;

public class ImitativeSingleton : Singleton
{
    private ImitativeSingleton() : base() { }

    public static Singleton GetInstance()
    {
        return Instance;
    }
}
