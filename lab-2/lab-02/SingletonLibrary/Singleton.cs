﻿namespace SingletonLibrary;

public class Singleton
{
   
    private static Singleton? _instance = null;
    private static readonly object _instanceLock = new();
    protected Singleton() { }
    public static Singleton Instance
    {
        get
        {
            if (_instance == null)
            {
                lock (_instanceLock)
                {
                    if (_instance == null)
                    {
                        _instance = new Singleton();
                    }
                }
            }
            return _instance;
        }
    }
}