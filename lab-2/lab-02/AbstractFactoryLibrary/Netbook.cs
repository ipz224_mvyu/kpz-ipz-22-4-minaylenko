﻿namespace AbstractFactoryLibrary;

public class Netbook
{
    public string Model { get; set; }
    public string Processor { get; set; }
    public int RamSize { get; set; }
    public string OperatingSystem { get; set; }

    public Netbook(string model, string processor, int ramSize, string operatingSystem)
    {
        Model = model;
        Processor = processor;
        RamSize = ramSize;
        OperatingSystem = operatingSystem;
    }

    public virtual string DisplayInfo()
    {
        return $"| - Netbook: Model: {Model}, Processor: {Processor}, RAM: {RamSize}GB, OS: {OperatingSystem}";
    }
}
