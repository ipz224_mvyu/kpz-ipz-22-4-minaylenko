﻿namespace AbstractFactoryLibrary;

public class IProneFactory : IDeviceFactory
{
    public Laptop CreateLaptop()
    {
        return new Laptop("IProne", "IProne1", "Intel Core i7-1165G7", 64, "Linux");
    }

    public Netbook CreateNetbook()
    {
        return new Netbook("IProne Netbook", "AMD Ryzen 7", 16, "Windows");
    }

    public EBook CreateEBook()
    {
        return new EBook("IProne Ebook", 8);
    }

    public Smartphone CreateSmartphone()
    {
        return new Smartphone("IProne Phone", "iOS", "Apple A15 Bionic", 6, 128, 6.1, 3400);
    }
}
