﻿namespace AbstractFactoryLibrary;

public class Smartphone
{
    public string Model { get; set; }
    public string OperatingSystem { get; set; }
    public string Processor { get; set; }
    public int RamSize { get; set; }
    public int StorageSize { get; set; }
    public double ScreenSize { get; set; }
    public int BatteryCapacity { get; set; }

    public Smartphone(string model, string operatingSystem, string processor, int ramSize, int storageSize, double screenSize, int batteryCapacity)
    {
        Model = model;
        OperatingSystem = operatingSystem;
        Processor = processor;
        RamSize = ramSize;
        StorageSize = storageSize;
        ScreenSize = screenSize;
        BatteryCapacity = batteryCapacity;
    }

    public virtual string DisplayInfo()
    {
        return $"| - Smartphone: Model: {Model}, OS: {OperatingSystem}, Processor: {Processor}, RAM: {RamSize}GB, Storage: {StorageSize}GB, Screen: {ScreenSize}, Battery: {BatteryCapacity}mAh";
    }
}
