﻿namespace AbstractFactoryLibrary;

public class KiaomiFactory : IDeviceFactory
{
    public Laptop CreateLaptop()
    {
        return new Laptop("Kiaomi", "Kiaomi1", "Intel Core i5-1165G7", 32, "Windows");
    }

    public Netbook CreateNetbook()
    {
        return new Netbook("Kiaomi Netbook", "AMD Ryzen 9", 8, "Windows");
    }

    public EBook CreateEBook()
    {
        return new EBook("Kiaomi Ebook", 13);
    }

    public Smartphone CreateSmartphone()
    {
        return new Smartphone("Kiaomi Phone", "Android", "Snapdragon 888", 8, 256, 6.2, 4000);
    }
}
