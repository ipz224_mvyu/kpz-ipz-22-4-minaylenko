﻿namespace AbstractFactoryLibrary
{
    public class Laptop
    {
        public string Brand { get; set; }
        public string Model { get; set; }
        public string Processor { get; set; }
        public int RamSize { get; set; }
        public string OperatingSystem { get; set; }

        public Laptop(string brand, string model, string processor, int ramSize, string operatingSystem)
        {
            Brand = brand;
            Model = model;
            Processor = processor;
            RamSize = ramSize;
            OperatingSystem = operatingSystem;
        }

        public virtual string DisplayInfo()
        {
            return $"| - Laptop: Brand: {Brand}, Model: {Model}, Processor: {Processor}, RAM: {RamSize}GB, OS: {OperatingSystem}";
        }
    }
}
