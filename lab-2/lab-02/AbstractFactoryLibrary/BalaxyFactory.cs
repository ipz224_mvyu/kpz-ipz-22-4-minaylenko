﻿namespace AbstractFactoryLibrary;

public class BalaxyFactory : IDeviceFactory
{
    public Laptop CreateLaptop()
    {
        return new Laptop("Balaxy", "Balaxy1", "AMD Ryzen 7 5800U", 16, "Linux");
    }

    public Netbook CreateNetbook()
    {
        return new Netbook("Balaxy Netbook", "Intel Core i9", 8, "Windows");
    }

    public EBook CreateEBook()
    {
        return new EBook("Balaxy Ebook", 32);
    }

    public Smartphone CreateSmartphone()
    {
        return new Smartphone("Balaxy Phone", "Android", "Google Tensor", 8, 128, 6.4, 4200);
    }
}
