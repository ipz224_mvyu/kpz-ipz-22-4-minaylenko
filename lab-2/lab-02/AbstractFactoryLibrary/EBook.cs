﻿namespace AbstractFactoryLibrary;
public class EBook
{
    public string Model { get; set; }
    public int StorageSize { get; set; }


    public EBook(string model, int storageSize)
    {
        Model = model;
        StorageSize = storageSize;
    }

    public virtual string DisplayInfo()
    {
        return $"| - EBook: Model: {Model}, Storage: {StorageSize}GB";
    }
}
