﻿using SubscriptionFabricLibrary;

namespace SubscriptionConsole
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ISubscriptionCreator websiteFactory = new WebSite();
            Subscription websiteSubscription = websiteFactory.CreateSubscription();
            websiteSubscription.DisplaySubscriptionInfo();

            ISubscriptionCreator mobileAppFactory = new MobileApp();
            Subscription mobileAppSubscription = mobileAppFactory.CreateSubscription();
            mobileAppSubscription.DisplaySubscriptionInfo();

            ISubscriptionCreator managerCallFactory = new ManagerCall();
            Subscription managerCallSubscription = managerCallFactory.CreateSubscription();
            managerCallSubscription.DisplaySubscriptionInfo();
        }
    }
}