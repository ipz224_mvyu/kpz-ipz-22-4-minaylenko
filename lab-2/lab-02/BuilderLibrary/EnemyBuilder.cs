﻿namespace BuilderLibrary;

public class EnemyBuilder : ICharacterBuilder
{
    private Character _character;

    public EnemyBuilder()
    {
        this.Reset();
    }

    public void Reset()
    {
        this._character = new Character();
    }

    public void SetHeight(double height)
    {
        this._character.Height = height;
    }

    public void SetBuild(string build)
    {
        this._character.Build = build;
    }

    public void SetHairColor(string hairColor)
    {
        this._character.HairColor = hairColor;
    }

    public void SetEyeColor(string eyeColor)
    {
        this._character.EyeColor = eyeColor;
    }

    public void SetClothing(string clothing)
    {
        this._character.Clothing = clothing;
    }

    public void SetInventory(List<string> inventory)
    {
        this._character.Inventory = inventory;
    }

    public void SetSpecialAbility(string specialAbility)
    {
        this._character.SpecialAbility = specialAbility;
    }

    public void SetLevel(int level)
    {
        this._character.Level = level;
    }

    public void SetEvilDeeds(List<string> evilDeeds)
    {
        this._character.EvilDeeds = evilDeeds;
    }

    public void SetEvilCharacteristics(string evilCharacteristics)
    {
        this._character.EvilCharacteristics = evilCharacteristics;
    }

    public Character Build()
    {
        Character result = this._character;
        this.Reset();
        return result;
    }
}
