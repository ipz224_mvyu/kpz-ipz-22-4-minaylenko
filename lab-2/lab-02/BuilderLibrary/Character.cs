﻿using System.Text;

namespace BuilderLibrary;

public class Character 
{
    public double Height { get; set; }
    public string Build { get; set; }
    public string HairColor { get; set; }
    public string EyeColor { get; set; }
    public string Clothing { get; set; }
    public List<string> Inventory { get; set; }
    public string SpecialAbility { get; set; }
    public int Level { get; set; }
    public List<string> EvilDeeds { get; set; }
    public string EvilCharacteristics { get; set; }

    public string DisplayInfo()
    {
        
        return $"| Height: {Height} \n" +
            $"| Build: {Build} \n" +
            $"| Hair Color: {HairColor} \n" +
            $"| Eye Color: {EyeColor} \n" +
            $"| Clothing: {Clothing} \n" +
            $"| Inventory: {string.Join(", ", Inventory)} \n" +
            $"| Special Ability: {SpecialAbility} \n" +
            $"| Level: {Level}  \n";
    }
}
