﻿namespace BuilderLibrary;

public class CharacterDirector
{
    private ICharacterBuilder _characterBuilder;

    public ICharacterBuilder CharacterBuilder
    {
        set { _characterBuilder = value; }
    }

    public Character ConstructCharacter(double height, string build, string hairColor, string eyeColor, string clothing, List<string> inventory, string specialAbility, int level)
    {
        if (_characterBuilder == null)
        {
            throw new InvalidOperationException("Character builder is not set.");
        }

        _characterBuilder.Reset();
        _characterBuilder.SetHeight(height);
        _characterBuilder.SetBuild(build);
        _characterBuilder.SetHairColor(hairColor);
        _characterBuilder.SetEyeColor(eyeColor);
        _characterBuilder.SetClothing(clothing);
        _characterBuilder.SetInventory(inventory);
        _characterBuilder.SetSpecialAbility(specialAbility);
        _characterBuilder.SetLevel(level);

        return _characterBuilder.Build();
    }

    public Character ConstructEnemy(double height, string build, string hairColor, string eyeColor, string clothing, List<string> inventory, string specialAbility, int level, List<string> evilDeeds, string evilCharacteristics)
    {
        if (_characterBuilder == null)
        {
            throw new InvalidOperationException("Character builder is not set.");
        }

        if (_characterBuilder is EnemyBuilder enemyBuilder) {

            enemyBuilder.Reset();
            enemyBuilder.SetHeight(height);
            enemyBuilder.SetBuild(build);
            enemyBuilder.SetHairColor(hairColor);
            enemyBuilder.SetEyeColor(eyeColor);
            enemyBuilder.SetClothing(clothing);
            enemyBuilder.SetInventory(inventory);
            enemyBuilder.SetSpecialAbility(specialAbility);
            enemyBuilder.SetLevel(level);
            enemyBuilder.SetEvilDeeds(evilDeeds);
            enemyBuilder.SetEvilCharacteristics(evilCharacteristics);


            return enemyBuilder.Build();
        }
        else
        {
            throw new InvalidOperationException("Character builder is not an instance of EnemyBuilder.");
        }
    }
}
