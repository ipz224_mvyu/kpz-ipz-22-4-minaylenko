﻿namespace BuilderLibrary;

public interface ICharacterBuilder
{
    void SetHeight(double height);
    void SetBuild(string build);
    void SetHairColor(string hairColor);
    void SetEyeColor(string eyeColor);
    void SetClothing(string clothing);
    void SetInventory(List<string> inventory);
    void SetSpecialAbility(string specialAbility);
    void SetLevel(int level);
    Character Build();
    void Reset();
}
