﻿using SingletonLibrary;

namespace SingletonConsole
{
    internal class Program
    {
        static void Main(string[] args) 
        {
            Singleton singl1 = Singleton.Instance;
            Singleton singl2 = Singleton.Instance;
            Singleton extendedAuth = ImitativeSingleton.GetInstance();
            Console.WriteLine($"Comparison: {singl1 == singl2}");
            Console.WriteLine($"Comparing an instance of a class and a class that inherits from it: {extendedAuth == singl1}");

            Console.ReadLine();

        }
    }
}